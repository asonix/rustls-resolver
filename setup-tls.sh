#!/usr/bin/env bash

set -xe

certstrap init --common-name exampleCA
certstrap request-cert --common-name example --domain localhost
certstrap sign example --CA exampleCA
