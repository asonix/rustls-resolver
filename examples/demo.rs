use std::time::Duration;

use actix_web::{web, App, HttpServer};

async fn index() -> &'static str {
    "Hewwo Mr Obama"
}

#[actix_web::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    rustls::crypto::aws_lc_rs::default_provider()
        .install_default()
        .expect("Set provider");

    let initial_key = read_key().await?.unwrap();

    let (tx, rx) = rustls_channel_resolver::channel::<32>(initial_key);

    let handle = actix_web::rt::spawn(async move {
        let mut interval = actix_web::rt::time::interval(Duration::from_secs(30));
        interval.tick().await;

        loop {
            interval.tick().await;
            match read_key().await {
                Ok(Some(key)) => tx.update(key),
                Ok(None) => eprintln!("No key in keyfile"),
                Err(e) => {
                    eprintln!("Failed to read key from fs {e}");
                }
            }
        }
    });

    let server_config = rustls::ServerConfig::builder()
        .with_no_client_auth()
        .with_cert_resolver(rx);

    HttpServer::new(|| App::new().route("/", web::get().to(index)))
        .bind_rustls_0_23("0.0.0.0:8443", server_config)?
        .bind("0.0.0.0:8080")?
        .run()
        .await?;

    handle.abort();
    let _ = handle.await;
    Ok(())
}

async fn read_key() -> Result<Option<rustls::sign::CertifiedKey>, Box<dyn std::error::Error>> {
    let cert_bytes = tokio::fs::read("./out/example.crt").await?;
    let certs = rustls_pemfile::certs(&mut cert_bytes.as_slice()).collect::<Result<Vec<_>, _>>()?;

    let key_bytes = tokio::fs::read("./out/example.key").await?;
    let Some(private_key) = rustls_pemfile::private_key(&mut key_bytes.as_slice())? else {
        return Ok(None);
    };

    let private_key = rustls::crypto::aws_lc_rs::sign::any_supported_type(&private_key)?;

    Ok(Some(rustls::sign::CertifiedKey::new(certs, private_key)))
}
